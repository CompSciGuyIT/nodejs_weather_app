const yargs = require('yargs');
const functions = require('./functions/functions');

const argv = yargs
    .options({
        a: {
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        },
        c: {
            alias: 'celcius',
            describe: 'displays temperature in degrees celcius',
            string: true
        },
        d: {
            alias: 'default',
            describe: 'Adds address as the default location',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

var encodedAddress;

const geoKey = 'AiIKIa0GetQKHMwF5buHrP22mr86y1D27kBk2zBDM9IzTjojC1HyCmIDtGFZjumP';

/* Check if user wants default address */
var getDefault = true;

process.argv.forEach((val) => {
    if (val === '-a' || val === '--address') {
        getDefault = false;
    }
});

if (getDefault) {

    functions.getDefaultAddress().then((results) => {
        encodedAddress = results;
        var geocodeURL = `https://dev.virtualearth.net/REST/v1/Locations?q=${encodedAddress}&key=${geoKey}`;
        
        functions.processData(geocodeURL, encodedAddress);
    }).catch((errorMessage) => {
        console.log(errorMessage);
    });


} else {
    encodedAddress = encodeURIComponent(argv.address);
    var geocodeURL = `https://dev.virtualearth.net/REST/v1/Locations?q=${encodedAddress}&key=${geoKey}`;
    functions.processData(geocodeURL, encodedAddress);
}



