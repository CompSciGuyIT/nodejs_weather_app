# NodeJS_Weather_App

Weather app to retrieve weather for a given location.

This is work that expanded on what was learned in the Udemy course 
'The Complete Node.js Developer Course (2nd Edition)' by Andrew Mead.

It was necessary to deviate from the original content when it ws found that the 
Google Maps API was no longer usable.  Instead Microsoft's Bing Maps API was used 
to retrieve a location's coordinates, which was then used in conjunction with the
Dark Sky Forecast API to get its weather information.

To run the program from the Linux command line you must have node 
and some other packages installed.

Acquiring NodeJS: 
Download the appropriate file from nodejs.org's downloads page, unpack and install.

It will also be necessary to install the yargs and axios packages.
Details regarding these packages, including version numbers and installation 
can be found at npmjs.com

Run the program: 
$ node weather-app.js -a "DESIRED LOCATION"

Bring up help: 
$ node weather-app.js -h